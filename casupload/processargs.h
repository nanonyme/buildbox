#ifndef INCLUDED_CASUPLOAD_PROCESSARGS
#define INCLUDED_CASUPLOAD_PROCESSARGS

#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_merklize.h>
#include <memory>
#include <string>
#include <vector>

namespace casupload {

struct ProcessedArgs {
    bool d_processed;
    bool d_valid;

    bool d_dryRunMode;
    buildboxcommon::LogLevel d_logLevel;

    buildboxcommon::ConnectionOptions d_casConnectionOptions;

    bool d_followSymlinks;
    std::vector<std::string> d_paths;
    std::string d_ignoreFile;
    std::string d_outputDigestFile;
};

ProcessedArgs processArgs(int argc, char *argv[]);

} // namespace casupload

#endif
