#casupload

`casupload` facilitates uploading blobs, action outputs and directory trees
to a previously configured Content Addressable Store and Action Cache (such
as BuildGrid, Buildbarn or Buildfarm).

## Usage

```
Usage: ./casupload
   --help                      Display usage end exit
   --remote                    URL for the CAS service [optional]
   --instance                  Name of the CAS instance [optional, default = ""]
   --server-cert               Public server certificate for CAS TLS (PEM-encoded) [optional]
   --client-key                Private client key for CAS TLS (PEM-encoded) [optional]
   --client-cert               Private client certificate for CAS TLS (PEM-encoded) [optional]
   --access-token              Access Token for authentication CAS (e.g. JWT, OAuth access token, etc), will be included as an HTTP Authorization bearer token [optional]
   --token-reload-interval     How long to wait before refreshing access token [optional]
   --googleapi-auth            Use GoogleAPIAuth for CAS service [optional, default = false]
   --retry-limit               Number of times to retry on grpc errors for CAS service [optional, default = "4"]
   --retry-delay               How long to wait in milliseconds before the first grpc retry for CAS service [optional, default = "1000"]
   --request-timeout           Sets the timeout for gRPC requests in seconds. Set to 0 for no timeout. [optional, default = "0"]
   --min-throughput            Sets the minimum throughput for gRPC requests in bytes per seconds.
                                  The value may be suffixed with K, M, G or T. [optional, default = "0"]
   --keepalive-time            Sets the period for gRPC keepalive pings in seconds. Set to 0 to disable keepalive pings. [optional, default = "0"]
   --load-balancing-policy     Which grpc load balancing policy to use for CAS service.
                                  Valid options are 'round_robin' and 'grpclb' [optional]
   --follow-symlinks           Follow symlinks in the inputs [optional]
   --dry-run                   Calculate and print digests, do not upload anything [optional]
   --output-digest-file        Write output digest to the file in the form "<HASH>/<SIZE_BYTES>" [optional]
   --ignore-file               Ignore files in the input directories that match the patterns in
                                  the specified file. Patterns format is as in .gitignore except
                                  ! and ** are not supported. [optional]
   --cas-server                [Deprecated] Use --remote [optional]
   --log-level                 Log level [optional, default = "error"]
   --verbose                   Set log level to 'debug' [optional]
                               POSITIONAL [required]

Uploads the given files and directories to CAS, then prints the digest
hash and size of the corresponding Directory messages.

The files are placed in CAS subdirectories corresponding to their
paths. For example, 'casupload file1.txt subdir/file2.txt' would
create a CAS directory containing file1.txt and a subdirectory called
'subdir' containing file2.txt.

The directories will be uploaded individually as merkle trees.
The merkle tree for a directory will contain all of the content
within the directory.

Example usage: casupload --remote=http://localhost:50051 foo.c foo.h
```
