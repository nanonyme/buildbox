#include <processargs.h>

#include <buildboxcommon_commandlinetypes.h>
#include <buildboxcommon_connectionoptions_commandline.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_logging_commandline.h>

#include <cstdlib>
#include <cstring>
#include <fstream>

namespace casupload {

using ArgumentSpec = buildboxcommon::CommandLineTypes::ArgumentSpec;
using DataType = buildboxcommon::CommandLineTypes::DataType;
using TypeInfo = buildboxcommon::CommandLineTypes::TypeInfo;
using DefaultValue = buildboxcommon::CommandLineTypes::DefaultValue;

const std::string usageText =
    "Uploads the given files and directories to CAS, then prints the digest\n"
    "hash and size of the corresponding Directory messages.\n"
    "\n"
    "The files are placed in CAS subdirectories corresponding to their\n"
    "paths. For example, 'casupload file1.txt subdir/file2.txt' would\n"
    "create a CAS directory containing file1.txt and a subdirectory called\n"
    "'subdir' containing file2.txt.\n"
    "\n"
    "The directories will be uploaded individually as merkle trees.\n"
    "The merkle tree for a directory will contain all of the content\n"
    "within the directory.\n"
    "\n"
    "Example usage: casupload --remote=http://localhost:50051 foo.c foo.h\n";

ProcessedArgs processArgs(int argc, char *argv[])
{
    ProcessedArgs args = {};

    // If this tool gets to use another API endpoint, this has to be changed
    // to be a common configuration, and separate CAS configuration added.
    //
    // NB: Third argument is not supplied because there is an obsolete
    // --cas-server argument that can be used instead of --remote.
    // Once --cas-server is removed, make --remote required by using
    // a three-argument constructor here.
    auto connectionOptionsCommandLine =
        buildboxcommon::ConnectionOptionsCommandLine("CAS", "");

    std::vector<buildboxcommon::CommandLineTypes::ArgumentSpec> spec;

    auto connectionOptionsSpec = connectionOptionsCommandLine.spec();

    spec.insert(spec.end(), connectionOptionsSpec.cbegin(),
                connectionOptionsSpec.cend());

    spec.emplace_back("follow-symlinks", "Follow symlinks in the inputs",
                      TypeInfo(DataType::COMMANDLINE_DT_BOOL),
                      ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITHOUT_ARG);

    spec.emplace_back("dry-run",
                      "Calculate and print digests, do not upload anything",
                      TypeInfo(DataType::COMMANDLINE_DT_BOOL),
                      ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITHOUT_ARG);

    spec.emplace_back(
        "output-digest-file",
        "Write output digest to the file in the form \"<HASH>/<SIZE_BYTES>\"",
        TypeInfo(DataType::COMMANDLINE_DT_STRING), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG);

    spec.emplace_back(
        "ignore-file",
        "Ignore files in the input directories that match the patterns in\n"
        "the specified file. Patterns format is as in .gitignore except\n"
        "! and ** are not supported.",
        TypeInfo(DataType::COMMANDLINE_DT_STRING), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG);

    spec.emplace_back("cas-server", "[Deprecated] Use --remote",
                      TypeInfo(DataType::COMMANDLINE_DT_STRING),
                      ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);

    auto loggingSpec = buildboxcommon::loggingCommandLineSpec();
    spec.insert(spec.end(), loggingSpec.cbegin(), loggingSpec.cend());

    spec.emplace_back("", "", TypeInfo(&args.d_paths),
                      ArgumentSpec::O_REQUIRED,
                      ArgumentSpec::C_WITH_REST_OF_ARGS);

    auto cl = buildboxcommon::CommandLine(spec, usageText);

    if (!cl.parse(argc, argv)) {
        cl.usage();
        return args;
    }

    if (cl.exists("help") || cl.exists("version")) {
        args.d_processed = true;
        return args;
    }

    if (!connectionOptionsCommandLine.configureChannel(
            cl, "", &args.d_casConnectionOptions)) {
        return args;
    }

    if (cl.exists("cas-server")) {
        std::cerr << "WARNING" << std::endl
                  << "WARNING --cas-server option is deprecated. Use --remote "
                     "instead."
                  << std::endl
                  << "WARNING" << std::endl;

        if (args.d_casConnectionOptions.d_url != "") {
            std::cerr << "WARNING" << std::endl
                      << "WARNING --cas-server and --remote options are "
                         "redundant, --cas-server value is ignored."
                      << std::endl
                      << "WARNING" << std::endl;
        }
        else {
            args.d_casConnectionOptions.d_url = cl.getString("cas-server");
        }
    }

    if (args.d_casConnectionOptions.d_url == "") {
        std::cerr << "One of --cas-server and --remote is required"
                  << std::endl;
        return args;
    }

    if (!buildboxcommon::parseLoggingOptions(cl, args.d_logLevel)) {
        return args;
    }

    args.d_valid = true;
    args.d_dryRunMode = cl.getBool("dry-run", false);
    args.d_followSymlinks = cl.getBool("follow-symlinks", false);
    args.d_ignoreFile = cl.getString("ignore-file", "");
    args.d_outputDigestFile = cl.getString("output-digest-file", "");
    return args;
} // namespace casupload

} // namespace casupload
