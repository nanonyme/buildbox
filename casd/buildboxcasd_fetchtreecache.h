/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_FETCHTREECACHE_H
#define INCLUDED_BUILDBOXCASD_FETCHTREECACHE_H

#include <buildboxcasd_digestcache.h>

#include <buildboxcommon_protos.h>

namespace buildboxcasd {

class FetchTreeCache {
    /*
     * Keeps a temporary cache with the root digests of the trees that were
     * fetched.
     *
     * Allows to store and query whether only the tree structure or the whole
     * tree's contents are available.
     */
  public:
    FetchTreeCache();

    void addRootDigest(const buildboxcommon::Digest &digest,
                       const bool with_files);

    bool hasRootDigest(const buildboxcommon::Digest &digest,
                       const bool with_files);

  private:
    DigestCache d_trees;
    DigestCache d_trees_with_files;

    static const unsigned int s_root_digest_ttl_seconds;
};

} // namespace buildboxcasd

#endif
