/*
 * Copyright 2024 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_LOCALEXECUTIONINSTANCE_H
#define INCLUDED_BUILDBOXCASD_LOCALEXECUTIONINSTANCE_H

#include <buildboxcasd_executioninstance.h>

#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_localexecutionclient.h>

#include <atomic>
#include <condition_variable>
#include <deque>
#include <memory>
#include <string>
#include <thread>
#include <vector>

using namespace build::bazel::remote::execution::v2;
using namespace google::longrunning;

namespace buildboxcasd {

class LocalExecutionInstance final : public ExecutionInstance {
  public:
    explicit LocalExecutionInstance(
        const buildboxcommon::ConnectionOptions &casdEndpoint,
        const std::string &runnerCommand,
        const std::vector<std::string> &extraRunArgs, int maxJobs);

    virtual ~LocalExecutionInstance();

    grpc::Status Execute(ServerContext *ctx, const ExecuteRequest &request,
                         ServerWriterInterface<Operation> *writer) override;

    grpc::Status
    WaitExecution(ServerContext *ctx, const WaitExecutionRequest &request,
                  ServerWriterInterface<Operation> *writer) override;

    grpc::Status GetOperation(const GetOperationRequest &request,
                              Operation *response) override;

    grpc::Status CancelOperation(const CancelOperationRequest &request,
                                 google::protobuf::Empty *response) override;

    void stop() override;

  private:
    struct InternalOperation {
        ExecuteRequest executeRequest;
        std::mutex mutex;
        Operation operation;
        std::condition_variable cv;
        bool queued = true;
        bool done = false;
    };

    void workerThread();

    grpc::Status
    waitExecutionInternal(ServerContext *ctx, const std::string &name,
                          std::shared_ptr<InternalOperation> op,
                          ServerWriterInterface<Operation> *writer);

    std::shared_ptr<buildboxcommon::LocalExecutionClient> d_execClient;

    std::vector<std::thread> d_workerThreads;

    std::mutex d_mutex;
    std::condition_variable d_cv;
    size_t d_workerThreadsWaiting = 0;
    std::atomic_bool d_stopRequested = false;
    std::deque<std::shared_ptr<InternalOperation>> d_queue;
    std::map<std::string, std::shared_ptr<InternalOperation>> d_operationMap;
};

} // namespace buildboxcasd

#endif // INCLUDED_BUILDBOXCASD_LOCALEXECUTIONINSTANCE_H
